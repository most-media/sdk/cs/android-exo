package com.mostmedia.bundle

import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.Timeline
import com.google.android.exoplayer2.source.dash.manifest.DashManifest
import com.google.android.exoplayer2.source.dash.manifest.Period
import com.google.android.exoplayer2.source.hls.HlsManifest
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.mostmedia.AdHandler
import com.mostmedia.AdListener
import com.mostmedia.Dash
import com.mostmedia.DefaultImaPlayer
import com.mostmedia.VideoImaPlayerFactory
import com.mostmedia.exo.AdVideoView
import java.lang.ref.WeakReference
import java.util.LinkedList
import java.util.Queue

class ExoPlayerAdHandler(
    private val playerView: AdVideoView,
    private val player: ExoPlayer,
    private val bandwidthMeter: BandwidthMeter,
    adTagParams: Map<String, String>,
    videoImaPlayerFactory: VideoImaPlayerFactory
) : AdHandler {
    private val imaPlayer = DefaultImaPlayer(
        playerView.adViewGroup!!,
        adTagParams,
        createAdListener(),
        videoImaPlayerFactory
    )
    private val bitrateStack: Queue<Int> = LinkedList()
    private val playerListener: Player.Listener
    private val bandwidthMeterListener: BandwidthMeter.EventListener
    private val handler: InnerHandler = InnerHandler(this)

    init {
        playerListener = createPlayerListener()
        player.addListener(playerListener)
        bandwidthMeterListener = createBandwidthMeterListener()
        bandwidthMeter.addEventListener(Handler(Looper.getMainLooper()), bandwidthMeterListener)

        if (!USE_EXO_PLAYER_PLAYBACK_EVENTS) {
            processStartAd()
        }
    }

    private fun createAdListener(): AdListener {
        return object : AdListener {
            override fun onAdLoaded(adBreakId: String, startAdPosition: Long) {
                if (USE_EXO_PLAYER_PLAYBACK_EVENTS) {
                    scheduleAdStart(adBreakId, startAdPosition)
                }
            }

            override fun onContentPauseRequested(adBreakId: String) {
                player.playWhenReady = false
            }

            override fun onContentResumeRequested(adBreakId: String?) {
                playerView.setPlayer(player)
                player.playWhenReady = true
            }

            override fun onContentSeekRequested(
                adBreakId: String,
                seekDuration: Int,
                seekPosition: Long
            ) {
                player.seekTo(seekPosition - getInitialPosition(player.currentTimeline))
            }
        }
    }

    private fun scheduleAdStart(adBreakId: String, startAdPosition: Long) {
        val playerPositionToStartAd = startAdPosition - getPlayerPosition().initial
        Log.d(
            MM_SCHEDULE_AD_START,
            "scheduleAdStart: adBreakId = $adBreakId, startAdPosition = $startAdPosition"
        )
        player
            .createMessage { messageType: Int, payload: Any? ->
                Log.d(MM_EXO_PLAYER_MESSAGE, "Player message for adBreakId = $adBreakId")
                imaPlayer.play(getPlayerPosition().current)
            }
            .setLooper(Looper.getMainLooper())
            .setPosition(playerPositionToStartAd)
            .setDeleteAfterDelivery(true)
            .send()
    }

    private fun createPlayerListener(): Player.Listener {

        return object : Player.Listener {
            override fun onTimelineChanged(timeline: Timeline, reason: Int) {

                val manifest = player.currentManifest ?: return
                val playerPosition = getPlayerPosition(timeline)
                if (playerPosition.initial == 0L) return

                val bitrateKbps = bitrateStack.average().toInt()
                Log.d(
                    MM_TIMELINE_CHANGE,
                    "onTimelineChanged: initialPosition = ${playerPosition.initial}, currentPosition = ${playerPosition.current}"
                )

                if (manifest is HlsManifest) {
                    val tags = manifest.mediaPlaylist.tags
                    imaPlayer.addOrUpdateAdBreaks(
                        tags,
                        playerPosition.initial,
                        bitrateKbps,
                        currentPosition = playerPosition.current
                    )
                }

                if (manifest is DashManifest) {
                    val periods = LinkedList<Period>()
                    for (i in 0 until manifest.periodCount)
                        periods.add(manifest.getPeriod(i))
                    val dashPeriods = periods.map { convertPeriod(it) }
                    imaPlayer.addOrUpdateAdBreaksForDash(Dash.extract(dashPeriods), bitrateKbps)
                }

            }

            fun convertPeriod(period: Period) = Dash.Period(
                period.id,
                period.startMs,
                period.eventStreams.map { es ->
                    Dash.EventStream(
                        es.timescale,
                        es.schemeIdUri,
                        es.events.map { em ->
                            Dash.EventMessage(
                                em.id,
                                em.durationMs
                            )
                        },
                        es.presentationTimesUs
                    )
                }
            )

        }

    }

    private fun createBandwidthMeterListener(): BandwidthMeter.EventListener {
        return BandwidthMeter.EventListener { _, _, bitrateEstimate ->
            bitrateStack.add((bitrateEstimate / 1000).toInt())
            while (bitrateStack.size > 20) {
                bitrateStack.remove()
            }
        }
    }

    private fun processStartAd() {
        val playerPosition = getPlayerPosition()
        imaPlayer.play(playerPosition.current)
        handler.sendMessageDelayed(handler.obtainMessage(MSG_START_AD_PROCESS), 5)
    }

    private fun getPlayerPosition() = getPlayerPosition(player.currentTimeline)

    private fun getPlayerPosition(timeline: Timeline): PlayerPosition {
        val initialPosition = getInitialPosition(timeline)
        val currentPosition = initialPosition + player.currentPosition
        return PlayerPosition(initialPosition, currentPosition)
    }

    private fun getInitialPosition(timeline: Timeline): Long {
        if (timeline.windowCount == 0) return 0
        return timeline
            .getWindow(player.currentWindowIndex, Timeline.Window())
            .positionInFirstPeriodUs / 1000
    }

    override fun stopAd() {
        imaPlayer.stop()
    }

    override fun getVolume(): Int {
        return imaPlayer.getVolume()
    }

    override fun setVolume(value: Int) {
        imaPlayer.setVolume(value)
    }

    override fun release() {
        handler.removeCallbacksAndMessages(null)
        player.removeListener(playerListener)
        bandwidthMeter.removeEventListener(bandwidthMeterListener)
        imaPlayer.release()
    }

    private class InnerHandler(adHandler: ExoPlayerAdHandler) : Handler(Looper.getMainLooper()) {
        private val weakAdHandler: WeakReference<ExoPlayerAdHandler> = WeakReference(adHandler)

        override fun handleMessage(message: Message) {
            val adHandler = weakAdHandler.get() ?: return
            when (message.what) {
                MSG_START_AD_PROCESS -> {
                    adHandler.processStartAd()
                }
            }
        }
    }

    private companion object {
        const val MM_TIMELINE_CHANGE = "MM_TIMELINE_CHANGE"
        const val MM_SCHEDULE_AD_START = "MM_SCHEDULE_AD_START"
        const val MM_EXO_PLAYER_MESSAGE = "MM_EXO_PLAYER_MESSAGE"
        const val MSG_START_AD_PROCESS = 1

        /***
         * Flag defines if ExoPlayer events fired at specified playback position should
         * be used for starting advertisements.
         */
        const val USE_EXO_PLAYER_PLAYBACK_EVENTS = true

        class PlayerPosition(
            val initial: Long,
            val current: Long
        )
    }
}
