package com.mostmedia.bundle

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.provider.Settings.Secure
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.PlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.mostmedia.AdHandler
import com.mostmedia.bundle.adsexoplayer.VideoImaExoPlayerFactory
import com.mostmedia.exo.AdVideoSurfaceView
import com.mostmedia.exo.AdVideoView

class PlayerActivity : AppCompatActivity() {
    private lateinit var adTagParams: HashMap<String, String>
    private lateinit var playerView: AdVideoView
    private var adHandler: AdHandler? = null
    private var player: ExoPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        playerView = AdVideoSurfaceView(findViewById(R.id.video_view))

        // create sample params for call MMSC
        adTagParams = createSampleAdTagParams()

        // sample change channel
        adTagParams["chID"] = "24"
    }

    // These params are provided for testing purposes only, and should be replaced with real values.
    // To obtain parameters for the other target groups, please contact the supplier.
    @SuppressLint("HardwareIds")
    private fun createSampleAdTagParams(): HashMap<String, String> {
        return hashMapOf(
            "devType" to "conTV",
            "devID" to Secure.getString(contentResolver, Secure.ANDROID_ID), // only for example
            "chID" to "001",
            "chTitle" to "CES001",
            "chGenre" to "Cartoon",
            "geoLat" to "22.5455400",
            "geoLon" to "114.0683000",
            "userAgent" to "Ktor client",
            "net" to "Net2Net"
        )
    }

    public override fun onStart() {
        super.onStart()
        if (Build.VERSION.SDK_INT > 23) {
            initializePlayer()
        }
    }

    public override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT <= 23 || player == null) {
            initializePlayer()
        }
    }

    public override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT > 23) {
            releasePlayer()
        }
    }

    private fun initializePlayer() {
        if (player != null) {
            return
        }

        val trackSelector = DefaultTrackSelector(this@PlayerActivity)
        val bandwidthMeter = DefaultBandwidthMeter.Builder(this@PlayerActivity).build()

        // When isDrm = false then HLS stream is used otherwise MPEG-DASH
        val isDrm = false
        val mediaItem = createMediaItem(isDrm)

        val newPlayer = ExoPlayer.Builder(this@PlayerActivity)
            .setTrackSelector(trackSelector)
            .setBandwidthMeter(bandwidthMeter).build()
        newPlayer.apply {
            addListener(object : Player.Listener {
                override fun onPlayerError(error: PlaybackException) {
                    releasePlayer()
                    initializePlayer()
                }
            })
            playWhenReady = true
            setMediaItem(mediaItem)
            prepare()
        }

        playerView.setPlayer(newPlayer)
        val videoImaPlayerFactory = VideoImaExoPlayerFactory(playerView)
        adHandler = ExoPlayerAdHandler(
            playerView,
            newPlayer,
            bandwidthMeter,
            adTagParams,
            videoImaPlayerFactory
        )
        if (false) { // !!! manual stop ad (call when switching channel)
            adHandler!!.stopAd()
        }
        player = newPlayer
    }

    /**
     * Creates MediaItem object.
     * If [isDrm] is true then MPEG-DASH stream is used, otherwise HLS.
     * Note: work with DRM requires ExoPlayer version 2.18.2 and above, but does not work on Android TV.
     */
    private fun createMediaItem(isDrm: Boolean): MediaItem {
        val mediaItemBuilder = MediaItem.Builder()
        var mediaUrl = ""
        if (isDrm) {
            // Requires exoplayer version 2.18.2 and above, but does not work on Android TV
            mediaUrl = getString(R.string.playlist_dash5)
            val license = getString(R.string.playlist_dash5_license)
            val config =
                MediaItem.DrmConfiguration.Builder(C.WIDEVINE_UUID)
                    .setLicenseUri(license)
                    .build()
            mediaItemBuilder.setDrmConfiguration(config)
        } else {
            mediaUrl = getString(R.string.playlist_hls1)
        }
        return mediaItemBuilder
            .setUri(mediaUrl)
            .build()
    }

    private fun releasePlayer() {
        player?.release()
        player = null

        // !!! release ad player
        adHandler?.release()
        adHandler = null
    }
}
