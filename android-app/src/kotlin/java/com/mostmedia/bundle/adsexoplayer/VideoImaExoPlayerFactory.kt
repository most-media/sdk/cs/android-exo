package com.mostmedia.bundle.adsexoplayer

import com.mostmedia.VideoImaPlayer
import com.mostmedia.VideoImaPlayerFactory
import com.mostmedia.VideoImaPlayerListener
import com.mostmedia.exo.AdVideoView

class VideoImaExoPlayerFactory(
    private val playerView: AdVideoView
) : VideoImaPlayerFactory {
    override fun create(listener: VideoImaPlayerListener): VideoImaPlayer {
        return VideoImaExoPlayer(playerView, listener)
    }
}