package com.mostmedia.bundle.adsexoplayer

import android.os.Handler
import android.os.Looper
import android.util.Log
import com.google.ads.interactivemedia.v3.api.AdPodInfo
import com.google.ads.interactivemedia.v3.api.player.AdMediaInfo
import com.google.ads.interactivemedia.v3.api.player.VideoAdPlayer.VideoAdPlayerCallback
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.PlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.util.EventLogger
import com.mostmedia.VideoImaPlayer
import com.mostmedia.VideoImaPlayerListener
import com.mostmedia.exo.AdVideoView
import java.util.Timer
import java.util.TimerTask
import kotlin.collections.ArrayList

class VideoImaExoPlayer(
    private val playerView: AdVideoView,
    private val listener: VideoImaPlayerListener
) : VideoImaPlayer, Player.Listener {
    private var currentAdMediaInfo: AdMediaInfo? = null
    private val player: SimpleExoPlayer
    private val adCallbacks = ArrayList<VideoAdPlayerCallback>(1)
    private var timer: Timer? = null

    init {
        val trackSelector = DefaultTrackSelector(playerView.context)
        val renderersFactory = DefaultRenderersFactory(playerView.context)
        player = SimpleExoPlayer.Builder(playerView.context, renderersFactory)
            .setTrackSelector(trackSelector)
            .build()
        player.addAnalyticsListener(EventLogger(trackSelector))
        player.addListener(this)
        player.playWhenReady = false
    }

    override fun loadAd(adMediaInfo: AdMediaInfo, adPodInfo: AdPodInfo) {
        val mediaItem = MediaItem.Builder().setUri(adMediaInfo.url).build()
        player.addMediaItem(mediaItem)
        if (player.mediaItemCount == 1) {
            player.prepare()
            player.playWhenReady = false
        }
        for (callback in adCallbacks) {
            callback.onLoaded(adMediaInfo)
        }
    }

    override fun playAd(adMediaInfo: AdMediaInfo) {
        if (currentAdMediaInfo == adMediaInfo) {
            return
        }
        currentAdMediaInfo = adMediaInfo
        playerView.setPlayer(player)
        Log.d(
            MM_PLAYER_BUFFER_TAG,
            "State of ad player buffer [sequenceNumber: 1, bufferPercentage: ${player.bufferedPercentage}]"
        )
        player.playWhenReady = true
        for (callback in adCallbacks) {
            callback.onPlay(adMediaInfo)
        }
        startTracking(adMediaInfo)
    }

    override fun onIsPlayingChanged(isPlaying: Boolean) {
        if (isPlaying) {
            listener.onPlayingStarted()
        }
    }

    override fun stopAd(adMediaInfo: AdMediaInfo) {
    }

    override fun pauseAd(adMediaInfo: AdMediaInfo) {
    }

    override fun addCallback(videoAdPlayerCallback: VideoAdPlayerCallback) {
        adCallbacks.add(videoAdPlayerCallback)
    }

    override fun removeCallback(videoAdPlayerCallback: VideoAdPlayerCallback) {
        adCallbacks.remove(videoAdPlayerCallback)
    }

    override fun getAdProgress(): VideoProgressUpdate {
        if (!player.playWhenReady || player.duration <= 0 ||
            player.playbackState == Player.STATE_IDLE && player.contentPosition > 0
        ) {
            return VideoProgressUpdate.VIDEO_TIME_NOT_READY
        }
        return VideoProgressUpdate(player.currentPosition, player.duration)
    }

    override fun getVolume(): Int {
        return (player.volume * 100).toInt()
    }

    override fun setVolume(value: Int) {
        val addMediaInfo = currentAdMediaInfo ?: return
        player.volume = value / 100f
        for (callback in adCallbacks) {
            callback.onVolumeChanged(addMediaInfo, value)
        }
    }

    override fun onPlayerError(error: PlaybackException) {
        val addMediaInfo = currentAdMediaInfo ?: return
        for (callback in adCallbacks) {
            callback.onError(addMediaInfo)
        }
    }

    override fun onPositionDiscontinuity(
        oldPosition: Player.PositionInfo,
        newPosition: Player.PositionInfo,
        reason: Int
    ) {
        stopTracking()
        val addMediaInfo = currentAdMediaInfo ?: return
        for (callback in adCallbacks) {
            callback.onEnded(addMediaInfo)
        }
    }

    override fun onPlaybackStateChanged(playbackState: Int) {
        val adMediaInfo = currentAdMediaInfo ?: return
        if (playbackState == Player.STATE_BUFFERING) {
            for (callback in adCallbacks) {
                callback.onBuffering(adMediaInfo)
            }
        } else if (playbackState == Player.STATE_ENDED) {
            for (callback in adCallbacks) {
                callback.onEnded(adMediaInfo)
            }
        }
    }

    override fun release() {
        currentAdMediaInfo = null
        player.release()
    }

    private fun startTracking(info: AdMediaInfo) {
        if (timer != null) {
            return
        }
        timer = Timer()
        val updateTimerTask: TimerTask = object : TimerTask() {
            override fun run() {
                for (callback in adCallbacks) {
                    Handler(Looper.getMainLooper()).post {
                        callback.onAdProgress(info, adProgress)
                    }
                }
            }
        }
        timer!!.schedule(updateTimerTask, 250, 250)
    }

    private fun stopTracking() {
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    private companion object {
        const val MM_PLAYER_BUFFER_TAG = "MM_PLAYER_BUFFER"
    }
}
