package com.mostmedia.bundle;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.mostmedia.AdHandler;
import com.mostmedia.VideoImaPlayerFactory;
import com.mostmedia.bundle.adsexoplayer.VideoImaExoPlayerFactory;
import com.mostmedia.exo.AdVideoSurfaceView;
import com.mostmedia.exo.AdVideoView;

import java.util.HashMap;

public final class PlayerActivity extends AppCompatActivity {
    private HashMap<String, String> adTagParams;
    private AdVideoView playerView;
    private AdHandler adHandler;
    private ExoPlayer player;

    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        playerView = new AdVideoSurfaceView(findViewById(R.id.video_view));

        // create sample params for call MMSC
        adTagParams = createSampleAdTagParams();

        // sample change channel
        adTagParams.put("chID", "24");
    }

    // These params are provided for testing purposes only, and should be replaced with real values.
    // To obtain parameters for the other target groups, please contact the supplier.
    @SuppressLint({"HardwareIds"})
    private HashMap<String, String> createSampleAdTagParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put("devType", "conTV");
        params.put("devID", Secure.getString(getContentResolver(), Secure.ANDROID_ID)); // only for example
        params.put("chID", "001");
        params.put("chTitle", "CES001");
        params.put("chGenre", "Cartoon");
        params.put("geoLat", "22.5455400");
        params.put("geoLon", "114.0683000");
        params.put("userAgent", "Ktor client");
        params.put("net", "Net2Net");
        return params;
    }

    public void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT > 23) {
            initializePlayer();
        }
    }

    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT <= 23 || player == null) {
            initializePlayer();
        }
    }

    public void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    public void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT > 23) {
            releasePlayer();
        }
    }

    private void initializePlayer() {
        if (player != null) {
            return;
        }

        Context context = playerView.getContext();
        DefaultTrackSelector trackSelector = new DefaultTrackSelector(context);
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter.Builder(context).build();

        player = new ExoPlayer.Builder(context)
                .setTrackSelector(trackSelector)
                .setBandwidthMeter(bandwidthMeter)
                .build();

        player.addListener(new Player.Listener() {
            public void onPlayerError(@NonNull PlaybackException error) {
                releasePlayer();
                initializePlayer();
            }
        });

        // When isDrm = false then HLS stream is used otherwise MPEG-DASH
        boolean isDrm = false;
        MediaItem mediaItem = createMediaItem(isDrm);

        player.setPlayWhenReady(true);
        player.setMediaItem(mediaItem);
        player.prepare();

        playerView.setPlayer(player);
        VideoImaPlayerFactory videoImaPlayerFactory = new VideoImaExoPlayerFactory(playerView);
        adHandler = new ExoPlayerAdHandler(playerView, player, bandwidthMeter, adTagParams, videoImaPlayerFactory);
        if (false) { // !!! manual stop ad (call when switching channel)
            adHandler.stopAd();
        }
    }

    /**
     * Creates MediaItem object.
     * If [isDrm] is true then MPEG-DASH stream is used, otherwise HLS.
     * Note: DRM does not work on Android TV.
     */
    @NonNull
    private MediaItem createMediaItem(boolean isDrm) {
        MediaItem.Builder mediaItemBuilder = new MediaItem.Builder();
        String mediaUrl;
        if (isDrm) {
            // Requires exoplayer version 2.18.2 and above, but does not work on Android TV
            mediaUrl = getString(R.string.playlist_dash5);
            String license = getString(R.string.playlist_dash5_license);
            MediaItem.DrmConfiguration config =
                    new MediaItem.DrmConfiguration.Builder(C.WIDEVINE_UUID)
                            .setLicenseUri(license)
                            .build();
            mediaItemBuilder.setDrmConfiguration(config);
        } else {
            mediaUrl = getString(R.string.playlist_hls1);
        }
        return mediaItemBuilder
                .setUri(mediaUrl)
                .build();
    }

    private void releasePlayer() {
        ExoPlayer playerLocal = player;
        if (playerLocal != null) {
            playerLocal.release();
            player = null;

            // !!! release ad player
            AdHandler adHandlerLocal = adHandler;
            if (adHandlerLocal != null) {
                adHandlerLocal.release();
                adHandler = null;
            }
        }
    }
}
