package com.mostmedia.bundle.adsexoplayer;

import androidx.annotation.NonNull;

import com.mostmedia.VideoImaPlayer;
import com.mostmedia.VideoImaPlayerFactory;
import com.mostmedia.VideoImaPlayerListener;
import com.mostmedia.exo.AdVideoView;

public final class VideoImaExoPlayerFactory implements VideoImaPlayerFactory {
    private final AdVideoView playerView;

    public VideoImaExoPlayerFactory(@NonNull AdVideoView playerView) {
        this.playerView = playerView;
    }

    @NonNull
    public VideoImaPlayer create(@NonNull VideoImaPlayerListener listener) {
        return new VideoImaExoPlayer(playerView, listener);
    }
}