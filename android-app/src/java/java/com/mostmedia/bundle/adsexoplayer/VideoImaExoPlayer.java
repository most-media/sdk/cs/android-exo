package com.mostmedia.bundle.adsexoplayer;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.ads.interactivemedia.v3.api.AdPodInfo;
import com.google.ads.interactivemedia.v3.api.player.AdMediaInfo;
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Player.Listener;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.util.EventLogger;

import com.mostmedia.VideoImaPlayer;
import com.mostmedia.VideoImaPlayerListener;
import com.mostmedia.exo.AdVideoView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public final class VideoImaExoPlayer implements VideoImaPlayer, Listener {

    private static final String MM_PLAYER_BUFFER_TAG = "MM_PLAYER_BUFFER";
    private final AdVideoView playerView;
    private final VideoImaPlayerListener listener;
    private AdMediaInfo currentAdMediaInfo;
    private final SimpleExoPlayer player;
    private final ArrayList<VideoAdPlayerCallback> adCallbacks = new ArrayList<>(1);
    private Timer timer;

    public VideoImaExoPlayer(@NonNull AdVideoView playerView, @NonNull VideoImaPlayerListener listener) {
        this.playerView = playerView;
        this.listener = listener;

        DefaultTrackSelector trackSelector = new DefaultTrackSelector(playerView.getContext());
        RenderersFactory renderersFactory = new DefaultRenderersFactory(playerView.getContext());
        player = new SimpleExoPlayer.Builder(playerView.getContext(), renderersFactory)
                .setTrackSelector(trackSelector)
                .build();
        player.addAnalyticsListener(new EventLogger(trackSelector));
        player.addListener(this);
        player.setPlayWhenReady(false);
    }

    public void loadAd(@NonNull AdMediaInfo adMediaInfo, @NonNull AdPodInfo adPodInfo) {
        MediaItem mediaItem = new MediaItem.Builder().setUri(adMediaInfo.getUrl()).build();
        player.addMediaItem(mediaItem);
        if (player.getMediaItemCount() == 1) {
            player.prepare();
            player.setPlayWhenReady(false);
        }
        for (VideoAdPlayerCallback callback : adCallbacks) {
            callback.onLoaded(adMediaInfo);
        }
    }

    public void playAd(@NonNull AdMediaInfo adMediaInfo) {
        if (currentAdMediaInfo == adMediaInfo) {
            return;
        }
        currentAdMediaInfo = adMediaInfo;
        playerView.setPlayer(player);
        Log.d(MM_PLAYER_BUFFER_TAG, "State of ad player buffer [sequenceNumber: 1, bufferPercentage: " + player.getBufferedPercentage() + ']');
        player.setPlayWhenReady(true);
        for (VideoAdPlayerCallback callback : adCallbacks) {
            callback.onPlay(adMediaInfo);
        }
        startTracking(adMediaInfo);
    }

    public void onIsPlayingChanged(boolean isPlaying) {
        if (isPlaying) {
            listener.onPlayingStarted();
        }
    }

    public void stopAd(@NonNull AdMediaInfo adMediaInfo) {
    }

    public void pauseAd(@NonNull AdMediaInfo adMediaInfo) {
    }

    public void addCallback(@NonNull VideoAdPlayerCallback videoAdPlayerCallback) {
        adCallbacks.add(videoAdPlayerCallback);
    }

    public void removeCallback(@NonNull VideoAdPlayerCallback videoAdPlayerCallback) {
        adCallbacks.remove(videoAdPlayerCallback);
    }

    @NonNull
    public VideoProgressUpdate getAdProgress() {
        if (!player.getPlayWhenReady() || player.getDuration() <= 0 ||
                player.getPlaybackState() == Player.STATE_IDLE && player.getContentPosition() > 0) {
            return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
        }
        return new VideoProgressUpdate(player.getCurrentPosition(), player.getDuration());
    }

    public int getVolume() {
        return (int) (player.getVolume() * 100);
    }

    public void setVolume(int value) {
        player.setVolume(value / 100F);
        for (VideoAdPlayerCallback callback : adCallbacks) {
            callback.onVolumeChanged(currentAdMediaInfo, value);
        }
    }

    public void onPlayerError(@NonNull PlaybackException error) {
        for (VideoAdPlayerCallback callback : adCallbacks) {
            callback.onError(currentAdMediaInfo);
        }
    }

    public void onPositionDiscontinuity(@NonNull Player.PositionInfo oldPosition, @NonNull Player.PositionInfo newPosition, int reason) {
        stopTracking();
        for (VideoAdPlayerCallback callback : adCallbacks) {
            callback.onEnded(currentAdMediaInfo);
        }
    }

    public void onPlaybackStateChanged(int playbackState) {
        if (playbackState == Player.STATE_BUFFERING) {
            for (VideoAdPlayerCallback callback : adCallbacks) {
                callback.onBuffering(currentAdMediaInfo);
            }
        }
        if (playbackState == Player.STATE_ENDED) {
            for (VideoAdPlayerCallback callback : adCallbacks) {
                callback.onEnded(currentAdMediaInfo);
            }
        }
    }

    public void release() {
        currentAdMediaInfo = null;
        player.release();
    }

    private void startTracking(final AdMediaInfo info) {
        if (timer != null) {
            return;
        }
        timer = new Timer();
        TimerTask updateTimerTask = new TimerTask() {
            public void run() {
                for (VideoAdPlayerCallback callback : adCallbacks) {
                    new Handler(Looper.getMainLooper()).post(() ->
                            callback.onAdProgress(info, getAdProgress()));
                }
            }
        };
        timer.schedule(updateTimerTask, 250, 250);
    }

    private void stopTracking() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
}
