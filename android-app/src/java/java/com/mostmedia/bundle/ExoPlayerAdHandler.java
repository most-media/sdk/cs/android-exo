package com.mostmedia.bundle;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Player.Listener;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.Timeline.Window;
import com.google.android.exoplayer2.metadata.emsg.EventMessage;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.source.dash.manifest.EventStream;
import com.google.android.exoplayer2.source.dash.manifest.Period;
import com.google.android.exoplayer2.source.hls.HlsManifest;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.BandwidthMeter.EventListener;

import com.mostmedia.AdHandler;
import com.mostmedia.AdListener;
import com.mostmedia.Dash;
import com.mostmedia.DefaultImaPlayer;
import com.mostmedia.VideoImaPlayerFactory;
import com.mostmedia.exo.AdVideoView;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

public final class ExoPlayerAdHandler implements AdHandler {

    private static final String MM_TIMELINE_CHANGE_TAG = "MM_TIMELINE_CHANGE";
    private static final String MM_SCHEDULE_AD_START_TAG = "MM_SCHEDULE_AD_START";
    private static final String MM_EXO_PLAYER_MESSAGE_TAG = "MM_EXO_PLAYER_MESSAGE";
    private static final int MSG_START_AD_PROCESS = 1;
    /***
     * Flag defines if ExoPlayer events fired at specified playback position should
     * be used for starting advertisements.
     */
    private static final boolean USE_EXO_PLAYER_PLAYBACK_EVENTS = true;

    private final DefaultImaPlayer imaPlayer;
    private final Queue<Integer> bitrateStack = new LinkedList<>();
    private final Listener playerListener;
    private final EventListener bandwidthMeterListener;
    private final ExoPlayerAdHandler.InnerHandler handler;
    private final AdVideoView playerView;
    private final ExoPlayer player;
    private final BandwidthMeter bandwidthMeter;

    public ExoPlayerAdHandler(@NonNull AdVideoView playerView,
                              @NonNull ExoPlayer player,
                              @NonNull BandwidthMeter bandwidthMeter,
                              @NonNull Map<String, String> adTagParams,
                              @NonNull VideoImaPlayerFactory videoImaPlayerFactory) {
        this.playerView = playerView;
        this.player = player;
        this.bandwidthMeter = bandwidthMeter;

        imaPlayer = new DefaultImaPlayer(Objects.requireNonNull(playerView.getAdViewGroup()), adTagParams, createAdListener(), videoImaPlayerFactory);
        handler = new ExoPlayerAdHandler.InnerHandler(this);

        playerListener = createPlayerListener();
        player.addListener(playerListener);
        bandwidthMeterListener = createBandwidthMeterListener();
        bandwidthMeter.addEventListener(new Handler(Looper.getMainLooper()), bandwidthMeterListener);

        if (!USE_EXO_PLAYER_PLAYBACK_EVENTS) {
            processStartAd();
        }
    }

    private AdListener createAdListener() {
        return new AdListener() {

            @Override
            public void onAdLoaded(@NonNull String adBreakId, long startAdPosition) {
                if (USE_EXO_PLAYER_PLAYBACK_EVENTS) {
                    scheduleAdStart(adBreakId, startAdPosition);
                }
            }

            @Override
            public void onContentPauseRequested(@NonNull String adBreakId) {
                player.setPlayWhenReady(false);
            }

            @Override
            public void onContentResumeRequested(@Nullable String adBreakId) {
                playerView.setPlayer(player);
                player.setPlayWhenReady(true);
            }

            @Override
            public void onContentSeekRequested(@NonNull String adBreakId, int seekDuration, long seekPosition) {
                player.seekTo(seekPosition - getInitialPosition(player.getCurrentTimeline()));
            }
        };
    }

    private void scheduleAdStart(@NonNull String adBreakId, long startAdPosition) {
        long playerPositionToStartAd = startAdPosition - getPlayerPosition().initial;
        Log.d(MM_SCHEDULE_AD_START_TAG,
                String.format("scheduleAdStart: adBreakId = %s, startAdPosition = %d", adBreakId, startAdPosition));
        player
                .createMessage(
                        (messageType, message) -> {
                            Log.d(MM_EXO_PLAYER_MESSAGE_TAG, "Player message for adBreakId = " + adBreakId);
                            imaPlayer.play(getPlayerPosition().current);
                        }
                )
                .setLooper(Looper.getMainLooper())
                .setPosition(playerPositionToStartAd)
                .setDeleteAfterDelivery(true)
                .send();
    }

    private Listener createPlayerListener() {

        return new Listener() {
            public void onTimelineChanged(@NonNull Timeline timeline, int reason) {

                Object manifest = player.getCurrentManifest();
                if (manifest == null) return;

                PlayerPosition playerPosition = getPlayerPosition();
                if (playerPosition.initial == 0L) return;

                double bitrateSum = 0.0;
                for (Integer bitrateItem : bitrateStack) {
                    bitrateSum += bitrateItem;
                }

                int bitrateKbps = (int) (bitrateSum / bitrateStack.size());
                Log.d(MM_TIMELINE_CHANGE_TAG,
                        String.format("onTimelineChanged: initialPosition = %d, currentPosition = %d",
                                playerPosition.initial, playerPosition.current));

                if (manifest instanceof HlsManifest hlsManifest) {
                    List<String> tags = hlsManifest.mediaPlaylist.tags;
                    imaPlayer.addOrUpdateAdBreaks(tags, playerPosition.initial, bitrateKbps, null, playerPosition.current);
                } else if (manifest instanceof DashManifest dashManifest) {
                    LinkedList<Dash.Period> dashPeriods = new LinkedList<>();
                    for (int i = 0; i < dashManifest.getPeriodCount(); i++) {
                        dashPeriods.add(convertPeriod(dashManifest.getPeriod(i)));
                    }
                    imaPlayer.addOrUpdateAdBreaksForDash(Dash.INSTANCE.extract(dashPeriods), bitrateKbps, null);
                }

            }

            Dash.Period convertPeriod(Period period) {
                LinkedList<Dash.EventStream> eventStreams = new LinkedList<>();
                for (EventStream eventStream : period.eventStreams) {
                    LinkedList<Dash.EventMessage> dem = new LinkedList<>();
                    for (EventMessage eventMessage : eventStream.events) {
                        dem.add(new Dash.EventMessage(eventMessage.id, eventMessage.durationMs));
                    }
                    eventStreams.add(new Dash.EventStream(
                            eventStream.timescale, eventStream.schemeIdUri, dem, eventStream.presentationTimesUs));
                }
                return new Dash.Period(period.id, period.startMs, eventStreams);
            }

        };

    }

    private EventListener createBandwidthMeterListener() {
        return (elapsedMs, bytesTransferred, bitrateEstimate) -> {
            bitrateStack.add((int) (bitrateEstimate / 1000));
            while (bitrateStack.size() > 20) {
                bitrateStack.remove();
            }
        };
    }

    private void processStartAd() {
        PlayerPosition playerPosition = getPlayerPosition();
        imaPlayer.play(playerPosition.current);
        handler.sendMessageDelayed(handler.obtainMessage(MSG_START_AD_PROCESS), 5);
    }

    @NonNull
    private PlayerPosition getPlayerPosition() {
        return getPlayerPosition(player.getCurrentTimeline());
    }

    @NonNull
    private PlayerPosition getPlayerPosition(@NonNull Timeline timeline) {
        long initialPosition = getInitialPosition(timeline);
        long currentPosition = initialPosition + player.getCurrentPosition();
        return new PlayerPosition(initialPosition, currentPosition);
    }

    private long getInitialPosition(Timeline timeline) {
        if (timeline.getWindowCount() == 0) return 0;
        return timeline
                .getWindow(player.getCurrentWindowIndex(), new Window())
                .getPositionInFirstPeriodUs() / 1000;
    }

    public void stopAd() {
        imaPlayer.stop();
    }

    public int getVolume() {
        return imaPlayer.getVolume();
    }

    public void setVolume(int value) {
        imaPlayer.setVolume(value);
    }

    public void release() {
        handler.removeCallbacksAndMessages(null);
        player.removeListener(playerListener);
        bandwidthMeter.removeEventListener(bandwidthMeterListener);
        imaPlayer.release();
    }

    private static final class PlayerPosition {
        public final long initial;
        public final long current;

        public PlayerPosition(long initialPos, long currentPos) {
            initial = initialPos;
            current = currentPos;
        }
    }

    private static final class InnerHandler extends Handler {
        private final WeakReference<ExoPlayerAdHandler> weakAdHandler;

        public InnerHandler(@NonNull ExoPlayerAdHandler adHandler) {
            super(Looper.getMainLooper());
            weakAdHandler = new WeakReference<>(adHandler);
        }

        public void handleMessage(@NonNull Message message) {
            ExoPlayerAdHandler adHandler = weakAdHandler.get();
            if (adHandler == null) return;

            if (message.what == MSG_START_AD_PROCESS) {
                adHandler.processStartAd();
            }
        }
    }
}
