# HOW TO RUN THE SAMPLE ANDROID APP

**TABLE OF CONTENTS**
* [HOW TO RUN THE SAMPLE ANDROID APP](#how-to-run-the-sample-android-app)
    * [1. STEP-BY-STEP MANUAL TO RUN THE SAMPLE APP](#1-step-by-step-manual-to-run-the-sample-app)
    * [2. AD INSERTION IN HLS AND MPEG-DASH LIVE STREAMS](#2-ad-insertion-in-hls-and-mpeg-dash-live-streams)
    * [3. AD INSERTION IN MPEG-DASH LIVE STREAMS WITH WIDEVINE DRM](#3-ad-insertion-in-mpeg-dash-live-streams-with-widevine-drm)
<br><br>

## 1. STEP-BY-STEP MANUAL TO RUN THE SAMPLE APP
### **1.1.** Download the project from gitLab to your local computer
### **1.2.** Open the project in Android Studio
### **1.3.** Depending on the language in which the application is being developed, select the build variant **(Menu Item) Build -> Select Build Variants... - >Active Build Variant**


### **1.4.** To implement the advertising SDK, fill in the repository access section in the **build.gradle** file at the **project level** with your data (login and password). Login and Password are provided upon request by sending a request to dmarkelov@mostmediaonline.com

**in the build.gradle on the project level**
```
mmMavenRepoUrl = "https://sdk-dai.mmcore.net/repository/maven"
mmMavenRepoUser = "LOGIN"
mmMavenRepoPassword = "PASSWORD"
```
### **1.5.** Synchronize **build.gradle** changes and Make a project
<br><br>

## 2. AD INSERTION IN HLS AND MPEG-DASH LIVE STREAMS
In order to use the solution with HLS or MPEG-DASH live streams, it is necessary to use in the **PlayerActivity** class:

**in the PlayerActivity**
```kotlin
val url = getString(R.string.playlist_hls1)  //  or val url = getString(R.string.playlist_dash1)
val mediaItem = MediaItem.Builder()
    .setUri(Uri.parse(url))
    .build()
```
<br><br>

## 3. AD INSERTION IN MPEG-DASH LIVE STREAMS WITH WIDEVINE DRM
In order to use the solution with MPEG-DASH live streams with WideVine DRM, it is necessary to use in the **PlayerActivity** class:

**in the PlayerActivity**
```kotlin
val url = getString(R.string.playlist_dash5)
val license = getString(R.string.playlist_dash5_license)
val config = MediaItem.DrmConfiguration.Builder(C.WIDEVINE_UUID)
    .setLicenseUri(license)
    .build()
val mediaItem = MediaItem.Builder()
    .setUri(Uri.parse(url))
    .setDrmConfiguration(config)
    .build()
```
<br><br>

**[REMARK]** It is important that full-fledged work with MPEG-DASH and WideVine DRM is implemented in the application in ExoPlayer version 2.18.2. Also, using DRM requires working with an application on Android Mobile and is not supported on Android TV.  However, the application code is provided as an implementation instruction and, as a rule, operators and integrators have their own approaches to using DRM functionality. This manual is not mandatory in this part.

